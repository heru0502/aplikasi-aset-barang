<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\Auth;
use App\Item;

class ItemController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $user = Auth::user();
        $items = Item::join('ref_rek_aset5', function ($join) {
            $join->on('ta_kib_b.Kd_Aset5', '=', 'ref_rek_aset5.Kd_Aset5');
            $join->on('ta_kib_b.Kd_Aset4', '=', 'ref_rek_aset5.Kd_Aset4');
            $join->on('ta_kib_b.Kd_Aset3', '=', 'ref_rek_aset5.Kd_Aset3');
            $join->on('ta_kib_b.Kd_Aset2', '=', 'ref_rek_aset5.Kd_Aset2');
            $join->on('ta_kib_b.Kd_Aset1', '=', 'ref_rek_aset5.Kd_Aset1');
        })->paginate(10);
        // dd($items);
        // $items = Item::with('field')->where('Kd_Bidang', $user->Kd_Bidang)->paginate(20);
        return view('item', compact('items', $items));        
    }

    public function search(Request $request)
    {
        $want_to_find = $request->want_to_find;
        $keyword = $request->keyword;

        if($request->want_to_find === 'kode lokasi') {
            $codes = explode('.', $request->keyword);

            $items = Item::join('ref_rek_aset5', function ($join) {
                $join->on('ta_kib_b.Kd_Aset5', '=', 'ref_rek_aset5.Kd_Aset5');
                $join->on('ta_kib_b.Kd_Aset4', '=', 'ref_rek_aset5.Kd_Aset4');
                $join->on('ta_kib_b.Kd_Aset3', '=', 'ref_rek_aset5.Kd_Aset3');
                $join->on('ta_kib_b.Kd_Aset2', '=', 'ref_rek_aset5.Kd_Aset2');
                $join->on('ta_kib_b.Kd_Aset1', '=', 'ref_rek_aset5.Kd_Aset1');
            })
            ->where([
                ['Kd_Pemilik', '=', intval($codes[0])],
                ['Kd_Prov', '=', intval($codes[1])],
                ['Kd_Kab_Kota', '=', intval($codes[2])],
                ['Kd_Bidang', '=', intval($codes[3])],
                ['Kd_Unit', '=', intval($codes[4])],
                [DB::raw("DATE_FORMAT(Tgl_Pembukuan, '%y')"), '=', $codes[5]],
                ['Kd_Sub', '=', intval($codes[6])],
            ])->get();
        }
        else if($request->want_to_find === 'kode barang') {
            $codes = explode('.', $request->keyword);

            $items = Item::join('ref_rek_aset5', function ($join) {
                $join->on('ta_kib_b.Kd_Aset5', '=', 'ref_rek_aset5.Kd_Aset5');
                $join->on('ta_kib_b.Kd_Aset4', '=', 'ref_rek_aset5.Kd_Aset4');
                $join->on('ta_kib_b.Kd_Aset3', '=', 'ref_rek_aset5.Kd_Aset3');
                $join->on('ta_kib_b.Kd_Aset2', '=', 'ref_rek_aset5.Kd_Aset2');
                $join->on('ta_kib_b.Kd_Aset1', '=', 'ref_rek_aset5.Kd_Aset1');
            })
            ->where([
                ['ta_kib_b.Kd_Aset1', '=', intval($codes[0])],
                ['ta_kib_b.Kd_Aset2', '=', intval($codes[1])],
                ['ta_kib_b.Kd_Aset3', '=', intval($codes[2])],
                ['ta_kib_b.Kd_Aset4', '=', intval($codes[3])],
                ['ta_kib_b.Kd_Aset5', '=', intval($codes[4])],
                ['ta_kib_b.No_Register', '=', intval($codes[5])],
            ])->get();
        }
        else if($request->want_to_find === 'nama barang') {

            $items = Item::join('ref_rek_aset5', function ($join) {
                $join->on('ta_kib_b.Kd_Aset5', '=', 'ref_rek_aset5.Kd_Aset5');
                $join->on('ta_kib_b.Kd_Aset4', '=', 'ref_rek_aset5.Kd_Aset4');
                $join->on('ta_kib_b.Kd_Aset3', '=', 'ref_rek_aset5.Kd_Aset3');
                $join->on('ta_kib_b.Kd_Aset2', '=', 'ref_rek_aset5.Kd_Aset2');
                $join->on('ta_kib_b.Kd_Aset1', '=', 'ref_rek_aset5.Kd_Aset1');
            })
            ->where('ref_rek_aset5.Nm_Aset5', 'like', "%$request->keyword%")->get();
        }
        
        return view('item', compact('items','want_to_find','keyword'));  
    }

    public function printQrcode($id)
    {
        $item = Item::join('ref_rek_aset5', function ($join) {
            $join->on('ta_kib_b.Kd_Aset5', '=', 'ref_rek_aset5.Kd_Aset5');
            $join->on('ta_kib_b.Kd_Aset4', '=', 'ref_rek_aset5.Kd_Aset4');
            $join->on('ta_kib_b.Kd_Aset3', '=', 'ref_rek_aset5.Kd_Aset3');
            $join->on('ta_kib_b.Kd_Aset2', '=', 'ref_rek_aset5.Kd_Aset2');
            $join->on('ta_kib_b.Kd_Aset1', '=', 'ref_rek_aset5.Kd_Aset1');
        })
        ->where('ta_kib_b.IDPemda', $id)->first();
        $pdf = \App::make('dompdf.wrapper');
        $pdf->loadView('print_qrcode_parent', compact('item'))->setPaper('a4', 'portrait');
        return $pdf->stream();
    }

    public function printQrcodeBatch(Request $request)
    {
        $items = Item::join('ref_rek_aset5', function ($join) {
            $join->on('ta_kib_b.Kd_Aset5', '=', 'ref_rek_aset5.Kd_Aset5');
            $join->on('ta_kib_b.Kd_Aset4', '=', 'ref_rek_aset5.Kd_Aset4');
            $join->on('ta_kib_b.Kd_Aset3', '=', 'ref_rek_aset5.Kd_Aset3');
            $join->on('ta_kib_b.Kd_Aset2', '=', 'ref_rek_aset5.Kd_Aset2');
            $join->on('ta_kib_b.Kd_Aset1', '=', 'ref_rek_aset5.Kd_Aset1');
        })
        ->whereIn('ta_kib_b.IDPemda', $request->selected_items)->get();
        $pdf = \App::make('dompdf.wrapper');
        $pdf->loadView('print_qrcode_parent', compact('items'))->setPaper('a4', 'portrait');
        return $pdf->stream();
    }

    public function convert_date()
    {
        ini_set('max_execution_time', 0);
        ini_set('memory_limit', '2G');
        
        DB::transaction(function () {
            $items = Item::all();

            foreach($items as $key => $item) {
                $split = explode(' ', $item->Tgl_Pembukuan);
                $split2 = explode('-', $split[0]);
                $date = $split2[2] .'-'. str_pad($split2[1], 2, '0', STR_PAD_LEFT) .'-'. str_pad($split2[0], 2, '0', STR_PAD_LEFT) .' '. $split[1];
                // $new_date[$key]['IDPemda'] = $item->IDPemda;
                // $new_date[$key]['Tgl_Pembukuan'] = str_replace('/', '-', $item->Tgl_Pembukuan);
                
                $item_new = Item::find($item->IDPemda);
                $item_new->Tgl_Pembukuan = $date;
                $item_new->update();
            }
            // dd($date);
        });
    }
}
