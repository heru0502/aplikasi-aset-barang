<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Models\Sector;
use App\Models\Unit;
use App\Models\SubUnit;
use App\Models\Upb;
use App\Models\Room;

class FilterController extends Controller
{
    public function index()
    {
        return response()->json(User::all());
    }

    public function getSectors()
    {
        return Sector::all();
    }

    public function getUnits($Kd_Bidang)
    {
        return Unit::where('Kd_Bidang', $Kd_Bidang)->get();
    }

    public function getSubUnits($Kd_Bidang, $Kd_Unit)
    {
        return SubUnit::where('Kd_Bidang', $Kd_Bidang)
                        ->where('Kd_Unit', $Kd_Unit)
                        ->get();
    }

    public function getUPB($Kd_Bidang, $Kd_Unit, $Kd_Sub)
    {
        return Upb::where('Kd_Bidang', $Kd_Bidang)
                        ->where('Kd_Unit', $Kd_Unit)
                        ->where('Kd_Sub', $Kd_Sub)
                        ->get();
    }

    public function getRooms($Kd_Bidang, $Kd_Unit, $Kd_Sub, $Kd_UPB)
    {
        return Room::where('Kd_Bidang', $Kd_Bidang)
                        ->where('Kd_Unit', $Kd_Unit)
                        ->where('Kd_Sub', $Kd_Sub)
                        ->where('Kd_UPB', $Kd_UPB)
                        ->get();
    }
}
