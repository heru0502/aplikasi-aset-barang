<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Illuminate\Support\Facades\Auth;
use App\Item;
use App\Models\Unit;
use App\Models\Room;

class KibBController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        $date_start = date('Y-m-d', strtotime($request->input('date_start')));
        $date_end = date('Y-m-d', strtotime($request->input('date_end')));
        $sector = $request->input('sector');
        $unit = $request->input('unit');
        $sub_unit = $request->input('sub_unit');
        $upb = $request->input('upb');
        $room = $request->input('room');
        
        $user = Auth::user();
        $items = Item::join('ref_rek_aset5', function ($join) {
            $join->on('ta_kib_b.Kd_Aset5', '=', 'ref_rek_aset5.Kd_Aset5');
            $join->on('ta_kib_b.Kd_Aset4', '=', 'ref_rek_aset5.Kd_Aset4');
            $join->on('ta_kib_b.Kd_Aset3', '=', 'ref_rek_aset5.Kd_Aset3');
            $join->on('ta_kib_b.Kd_Aset2', '=', 'ref_rek_aset5.Kd_Aset2');
            $join->on('ta_kib_b.Kd_Aset1', '=', 'ref_rek_aset5.Kd_Aset1');
        })
        ->where(function ($query) use ($date_start, $date_end, $sector, $unit, $sub_unit, $upb, $room) {
            if ($date_start && $date_end) $query->whereBetween('Tgl_Pembukuan', [$date_start, $date_end]);
            if ($sector) $query->where('Kd_Bidang', $sector);
            if ($unit) $query->where('Kd_Unit', $unit);
            if ($sub_unit) $query->where('Kd_Sub', $sub_unit);
            if ($upb) $query->where('Kd_UPB', $upb);
            if ($room) $query->where('Kd_Ruang', $room);
        })
        ->paginate(10);
        
        return view('admin.kib_b.index', compact('items', $items));        
    }

    public function search(Request $request)
    {
        $want_to_find = $request->want_to_find;
        $keyword = $request->keyword;

        if($request->want_to_find === 'kode lokasi') {
            $codes = explode('.', $request->keyword);

            $items = Item::join('ref_rek_aset5', function ($join) {
                $join->on('ta_kib_b.Kd_Aset5', '=', 'ref_rek_aset5.Kd_Aset5');
                $join->on('ta_kib_b.Kd_Aset4', '=', 'ref_rek_aset5.Kd_Aset4');
                $join->on('ta_kib_b.Kd_Aset3', '=', 'ref_rek_aset5.Kd_Aset3');
                $join->on('ta_kib_b.Kd_Aset2', '=', 'ref_rek_aset5.Kd_Aset2');
                $join->on('ta_kib_b.Kd_Aset1', '=', 'ref_rek_aset5.Kd_Aset1');
            })
            ->where([
                ['Kd_Pemilik', '=', intval($codes[0])],
                ['Kd_Prov', '=', intval($codes[1])],
                ['Kd_Kab_Kota', '=', intval($codes[2])],
                ['Kd_Bidang', '=', intval($codes[3])],
                ['Kd_Unit', '=', intval($codes[4])],
                [DB::raw("DATE_FORMAT(Tgl_Pembukuan, '%y')"), '=', $codes[5]],
                ['Kd_Sub', '=', intval($codes[6])],
            ])->get();
        }
        else if($request->want_to_find === 'kode barang') {
            $codes = explode('.', $request->keyword);

            $items = Item::join('ref_rek_aset5', function ($join) {
                $join->on('ta_kib_b.Kd_Aset5', '=', 'ref_rek_aset5.Kd_Aset5');
                $join->on('ta_kib_b.Kd_Aset4', '=', 'ref_rek_aset5.Kd_Aset4');
                $join->on('ta_kib_b.Kd_Aset3', '=', 'ref_rek_aset5.Kd_Aset3');
                $join->on('ta_kib_b.Kd_Aset2', '=', 'ref_rek_aset5.Kd_Aset2');
                $join->on('ta_kib_b.Kd_Aset1', '=', 'ref_rek_aset5.Kd_Aset1');
            })
            ->where([
                ['ta_kib_b.Kd_Aset1', '=', intval($codes[0])],
                ['ta_kib_b.Kd_Aset2', '=', intval($codes[1])],
                ['ta_kib_b.Kd_Aset3', '=', intval($codes[2])],
                ['ta_kib_b.Kd_Aset4', '=', intval($codes[3])],
                ['ta_kib_b.Kd_Aset5', '=', intval($codes[4])],
                ['ta_kib_b.No_Register', '=', intval($codes[5])],
            ])->get();
        }
        else if($request->want_to_find === 'nama barang') {

            $items = Item::join('ref_rek_aset5', function ($join) {
                $join->on('ta_kib_b.Kd_Aset5', '=', 'ref_rek_aset5.Kd_Aset5');
                $join->on('ta_kib_b.Kd_Aset4', '=', 'ref_rek_aset5.Kd_Aset4');
                $join->on('ta_kib_b.Kd_Aset3', '=', 'ref_rek_aset5.Kd_Aset3');
                $join->on('ta_kib_b.Kd_Aset2', '=', 'ref_rek_aset5.Kd_Aset2');
                $join->on('ta_kib_b.Kd_Aset1', '=', 'ref_rek_aset5.Kd_Aset1');
            })
            ->where('ref_rek_aset5.Nm_Aset5', 'like', "%$request->keyword%")->get();
        }
        
        return view('admin.kib-b.index', compact('items','want_to_find','keyword'));  
    }

    public function printQrcode($id)
    {
        $item = Item::join('ref_rek_aset5', function ($join) {
            $join->on('ta_kib_b.Kd_Aset5', '=', 'ref_rek_aset5.Kd_Aset5');
            $join->on('ta_kib_b.Kd_Aset4', '=', 'ref_rek_aset5.Kd_Aset4');
            $join->on('ta_kib_b.Kd_Aset3', '=', 'ref_rek_aset5.Kd_Aset3');
            $join->on('ta_kib_b.Kd_Aset2', '=', 'ref_rek_aset5.Kd_Aset2');
            $join->on('ta_kib_b.Kd_Aset1', '=', 'ref_rek_aset5.Kd_Aset1');
        })
        ->where('ta_kib_b.IDPemda', $id)->first();

        $unit = Unit::where([
            ['Kd_Prov', '=', $item->Kd_Prov],
            ['Kd_Kab_Kota', '=', $item->Kd_Kab_Kota],
            ['Kd_Bidang', '=', $item->Kd_Bidang],
            ['Kd_Unit', '=', $item->Kd_Unit]
        ])->first();

        $room = Room::where([
            ['Kd_Prov', '=', $item->Kd_Prov],
            ['Kd_Kab_Kota', '=', $item->Kd_Kab_Kota],
            ['Kd_Bidang', '=', $item->Kd_Bidang],
            ['Kd_Unit', '=', $item->Kd_Unit],
            ['Kd_Sub', '=', $item->Kd_Sub],
            ['Kd_UPB', '=', $item->Kd_UPB],
            ['Kd_Ruang', '=', $item->Kd_Ruang]
        ])->first();

        $pdf = \App::make('dompdf.wrapper');
        $pdf->loadView('print_qrcode_parent', compact('item','unit','room'))->setPaper('a4', 'portrait');
        return $pdf->stream();
    }

    public function printQrcodeBatch(Request $request)
    {
        foreach($request->selected_items as $key => $id) {
            $items[$key]['item'] = Item::join('ref_rek_aset5', function ($join) {
                $join->on('ta_kib_b.Kd_Aset5', '=', 'ref_rek_aset5.Kd_Aset5');
                $join->on('ta_kib_b.Kd_Aset4', '=', 'ref_rek_aset5.Kd_Aset4');
                $join->on('ta_kib_b.Kd_Aset3', '=', 'ref_rek_aset5.Kd_Aset3');
                $join->on('ta_kib_b.Kd_Aset2', '=', 'ref_rek_aset5.Kd_Aset2');
                $join->on('ta_kib_b.Kd_Aset1', '=', 'ref_rek_aset5.Kd_Aset1');
            })
            ->where('ta_kib_b.IDPemda', $id)->first();
    
            $items[$key]['unit'] = Unit::where([
                ['Kd_Prov', '=', $items[$key]['item']->Kd_Prov],
                ['Kd_Kab_Kota', '=', $items[$key]['item']->Kd_Kab_Kota],
                ['Kd_Bidang', '=', $items[$key]['item']->Kd_Bidang],
                ['Kd_Unit', '=', $items[$key]['item']->Kd_Unit]
            ])->first();
    
            $items[$key]['room'] = Room::where([
                ['Kd_Prov', '=', $items[$key]['item']->Kd_Prov],
                ['Kd_Kab_Kota', '=', $items[$key]['item']->Kd_Kab_Kota],
                ['Kd_Bidang', '=', $items[$key]['item']->Kd_Bidang],
                ['Kd_Unit', '=', $items[$key]['item']->Kd_Unit],
                ['Kd_Sub', '=', $items[$key]['item']->Kd_Sub],
                ['Kd_UPB', '=', $items[$key]['item']->Kd_UPB],
                ['Kd_Ruang', '=', $items[$key]['item']->Kd_Ruang]
            ])->first();
        }
        
        $pdf = \App::make('dompdf.wrapper');
        $pdf->loadView('print_qrcode_parent', compact('items'))->setPaper('a4', 'portrait');
        return $pdf->stream();
    }

    public function convert_date()
    {
        ini_set('max_execution_time', 0);
        ini_set('memory_limit', '2G');
        
        DB::transaction(function () {
            $items = Item::all();

            foreach($items as $key => $item) {
                $split = explode(' ', $item->Tgl_Pembukuan);
                $split2 = explode('-', $split[0]);
                $date = $split2[2] .'-'. str_pad($split2[1], 2, '0', STR_PAD_LEFT) .'-'. str_pad($split2[0], 2, '0', STR_PAD_LEFT) .' '. $split[1];

                $item_new = Item::find($item->IDPemda);
                $item_new->Tgl_Pembukuan = $date;
                $item_new->update();
            }
        });
    }
}
