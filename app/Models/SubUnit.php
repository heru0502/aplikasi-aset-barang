<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SubUnit extends Model
{
    protected $table = 'ref_sub_unit';
    protected $primaryKey = 'Kd_Sub';
    public $timestamps = false;
    public $incrementing = false;
}
