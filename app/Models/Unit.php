<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Unit extends Model
{
    protected $table = 'ref_unit';
    protected $primaryKey = 'Kd_Unit';
    public $timestamps = false;
    public $incrementing = false;
}
