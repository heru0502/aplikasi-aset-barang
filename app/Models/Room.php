<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Room extends Model
{
    protected $table = 'ta_ruang';
    protected $primaryKey = 'Kd_Ruang';
    public $timestamps = false;
    public $incrementing = false;
}
