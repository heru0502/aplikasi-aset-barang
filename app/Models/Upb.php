<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Upb extends Model
{
    protected $table = 'ta_upb';
    protected $primaryKey = 'Kd_UPB';
    public $timestamps = false;
    public $incrementing = false;
}
