<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Sector extends Model
{
    protected $table = 'ref_bidang';
    protected $primaryKey = 'Kd_Bidang';
    public $timestamps = false;
    public $incrementing = false;
}
