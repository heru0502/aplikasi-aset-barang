<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Item extends Model
{
    protected $table = 'ta_kib_b';
    protected $primaryKey = 'IDPemda';
    public $timestamps = false;
    public $incrementing = false;

    public function field()
    {
        return $this->belongsTo('App\Field', 'Kd_Bidang');
    }

    public function unit()
    {
        return $this->belongsTo('App\Models\Unit', 'Kd_Unit');
    }

    public function room()
    {
        return $this->belongsTo('App\Models\Room', 'Kd_Ruang');
    }
}
