<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Field extends Model
{
    protected $table = 'ref_bidang';
    protected $primaryKey = 'Kd_Bidang';
    public $timestamps = false;

    // public function item()
    // {
    //     return $this->hasOne('App\Item', 'Kd_Bidang');
    // }
}
