Requirement :
- PHP 7.2
- MySQL 5.7
- Laragon (recommended) or XAMPP
- Composer
- Git

How to install :
- Create database
- Import db sqlbmd
- Run **git clone https://gitlab.com/heru0502/aplikasi-aset-barang.git**
- Open directory prject.
- Rename **.env.example** to **.env**
- Adjust **APP_URL=http://aplikasi-aset-barang.test**
- Adjust **DB_DATABASE, DB_USERNAME, DB_PASSWORD**
- Run **composer update**
- Run **php artisan key:generate**
- Run **php artisan migrate --seed**

How tu use :
- open in browser **http://aplikasi-aset-barang.test**
- Account default | **username: heru0502@gmail.com** | **password: rahasia**