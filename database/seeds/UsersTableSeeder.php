<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            [
                'name' => 'heru firmansyah',
                'email' => 'heru0502@gmail.com',
                'password' => bcrypt('rahasia'),
                'Kd_Prov' => '25',
                'Kd_Kab_Kota' => '1',
                'Kd_Bidang' => '4',
                'Kd_Unit' => '1',
                'Kd_Sub' => '1',
                'Kd_UPB' => '2',
            ],
            [
                'name' => 'BPKAD',
                'email' => 'bpkad@banjarkab.go.id',
                'password' => bcrypt('rahasia'),
                'Kd_Prov' => '25',
                'Kd_Kab_Kota' => '1',
                'Kd_Bidang' => '4',
                'Kd_Unit' => '1',
                'Kd_Sub' => '1',
                'Kd_UPB' => '2',
            ]
        ]);
    }
}
