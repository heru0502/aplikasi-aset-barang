


<style>
    table {
        border-collapse: collapse;
        width: 100%;
        margin-bottom: 10px;
    }

    table, th, td {
        border: 1px solid black;
        padding: 10px;
        text-align: center;
    }

    .col-image {
        padding: 1px;
        widt
        h: 150px;
    }

    .page-break {
        page-break-after: always;
    }
</style>

@if($_SERVER['REQUEST_METHOD'] === 'POST')
    @foreach($items as $key => $item)
        @include('print_qrcode_child', [
            'item' => $item['item'], 
            'unit' => $item['unit'], 
            'room' => $item['room'],
            'key' => $key
        ])

        @if($key === 2)
            <div class="page-break"></div>
        @endif
    @endforeach
@else
    @include('print_qrcode_child', ['item' => $item])
@endif

