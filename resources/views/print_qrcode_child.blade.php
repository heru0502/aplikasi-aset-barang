<table>
    <tr>
        <td rowspan="4" class="col-image">
            <img src="{{ asset('images/logo_kab_banjar.png') }}" alt="Logo Kab Banjar" width="100">
        </td>
        <td colspan="7">KODE LOKASI</td>
        <td rowspan="4" class="col-image" style="padding-top: 15px;">
            <img src="data:image/png;base64, 
                {{ base64_encode(QrCode::format('png')->size(150)
                ->generate('Nama: '.$item->Nm_Aset5 .' | '. 'Tahun: '.date('Y', strtotime($item->Tgl_Pembukuan)) .' | '. 'Harga: Rp '.number_format($item->Harga, 2, ',','.') .' | '.
                'SKPD: '.$unit->Nm_Unit .' | '. 'Ruangan: '.$room->Nm_Ruang .' | '. 'Merk: '.$item->Merk)) }} ">
            <br>
            <span style="font-size:10pt">No. Urut : {{ $item->No_Register }}</span>
        </td>
    </tr>
    <tr>
        <td>{{ str_pad($item->Kd_Pemilik, 2, "0", STR_PAD_LEFT) }}</td>
        <td>{{ str_pad($item->Kd_Prov, 2, "0", STR_PAD_LEFT) }}</td>
        <td>{{ str_pad($item->Kd_Kab_Kota, 2, "0", STR_PAD_LEFT) }}</td>
        <td>{{ str_pad($item->Kd_Bidang, 2, "0", STR_PAD_LEFT) }}</td>
        <td>{{ str_pad($item->Kd_Unit, 2, "0", STR_PAD_LEFT) }}</td>
        <td>{{ date('y', strtotime($item->Tgl_Pembukuan)) }}</td>
        <td>{{ str_pad($item->Kd_Sub, 2, "0", STR_PAD_LEFT) }}</td>
    </tr>
    <tr>
        <td colspan="7">KODE BARANG</td>
    </tr>
    <tr>
        <td>{{ str_pad($item->Kd_Aset1, 2, "0", STR_PAD_LEFT) }}</td>
        <td>{{ str_pad($item->Kd_Aset2, 2, "0", STR_PAD_LEFT) }}</td>
        <td>{{ str_pad($item->Kd_Aset3, 2, "0", STR_PAD_LEFT) }}</td>
        <td>{{ str_pad($item->Kd_Aset4, 2, "0", STR_PAD_LEFT) }}</td>
        <td>{{ str_pad($item->Kd_Aset5, 2, "0", STR_PAD_LEFT) }}</td>
        <td colspan="2">{{ str_pad($item->No_Register, 4, "0", STR_PAD_LEFT) }}</td>
    </tr>
</table>