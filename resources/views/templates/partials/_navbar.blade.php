<nav class="navbar navbar-expand-lg main-navbar">
    <a href="index.html" class="navbar-brand sidebar-gone-hide">{{ config('app.name') }}</a>
    <div class="navbar-nav">
      	<a href="#" class="nav-link sidebar-gone-show" data-toggle="sidebar"><i class="fas fa-bars"></i></a>
    </div>
    <div class="nav-collapse">
		<a class="sidebar-gone-show nav-collapse-toggle nav-link" href="#">
			<i class="fas fa-ellipsis-v"></i>
		</a>
    </div>
    <form class="form-inline ml-auto">
		<ul class="navbar-nav">
			<li><a href="#" data-toggle="search" class="nav-link nav-link-lg d-sm-none"><i class="fas fa-search"></i></a></li>
		</ul>
    </form>
    <ul class="navbar-nav navbar-right">
      	<li><a href="{{ route('logout') }}" class="nav-link nav-link-lg"><i class="far fa-user"></i> Logout</a></li>
    </ul>
</nav>

<nav class="navbar navbar-secondary navbar-expand-lg">
    <div class="container">
      	<ul class="navbar-nav">
			<li class="nav-item {{ request()->is('admin/kib-b*') ? 'active' : '' }}">
				<a href="{{ route('kib-b') }}" class="nav-link"><i class="fas fa-fire"></i><span>KIB B</span></a>
			</li>
			<li class="nav-item {{ request()->is('admin/kir*') ? 'active' : '' }}">
				<a href="{{ route('kir') }}" class="nav-link"><i class="fas fa-fire"></i><span>KIR</span></a>
			</li>
      	</ul>
    </div>
</nav>