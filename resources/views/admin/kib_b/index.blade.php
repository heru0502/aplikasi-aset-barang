@extends('templates.default')

@section('content')
<section class="section">
    <div class="section-header">
		<h1>Kartu Inventaris Barang</h1>
	</div>
	
	<div class="section-body">
		<h2 class="section-title">Filter Aset</h2>
		<p class="section-lead">Isi parameter dibawah sesuai dengan aset barang apa yang ingin anda cari.</p>
		<div class="card">
			<div class="card-header">
				<h4>Filter</h4>
			</div>

			<div class="card-body p-3">
				<filter-component></filter-component>
			</div>
		</div>
	</div>

    <div class="section-body">
		<h2 class="section-title">Daftar Aset</h2>
		<p class="section-lead">Semua dafar aset maupun berdasarkan filter tertentu.</p>
		<div class="card">
			<div class="card-header">
				<h4>Daftar Aset</h4>
				<div class="card-header-form">
					<form action="{{ route('kib-b.search') }}" method="POST">
					@csrf

						<div class="row">
							<div class="col-xs-6">
								<div class="custom-control custom-radio custom-control-inline">
									<input type="radio" id="customRadioInline1" name="want_to_find" class="custom-control-input"value="kode lokasi" required="true" {{ !empty($want_to_find) && $want_to_find === 'kode lokasi' ? 'checked' : '' }}>
									<label class="custom-control-label" for="customRadioInline1">Kode Lokasi</label>
								</div>
								<div class="custom-control custom-radio custom-control-inline">
									<input type="radio" id="customRadioInline2" name="want_to_find" class="custom-control-input" value="kode barang" required="true" {{ !empty($want_to_find) && $want_to_find === 'kode barang' ? 'checked' : '' }}>
									<label class="custom-control-label" for="customRadioInline2">Kode Barang</label>
								</div>
								<div class="custom-control custom-radio custom-control-inline">
									<input type="radio" id="customRadioInline3" name="want_to_find" class="custom-control-input" value="nama barang" required="true" {{ !empty($want_to_find) && $want_to_find === 'nama barang' ? 'checked' : '' }}>
									<label class="custom-control-label" for="customRadioInline3">Nama Barang</label>
								</div>
							</div>
							<div class="col-xs-6">
								<div class="input-group">
									<input type="text" class="form-control" name="keyword" placeholder="Search" value="{{ $keyword ?? '' }}" required="true">
									<div class="input-group-btn">
										<button class="btn btn-primary"><i class="fas fa-search"></i></button>
									</div>
								</div>
							</div>							
						
						</div>
					</form>
				</div>
			</div>

			<form action="{{ route('print-qrcode-batch') }}" method="post" target="_blank">
				@csrf

				<div class="card-header">
					<div class="row">
						<button class="btn btn-warning" type="submit"><i class="fas fa-print"></i> Cetak Banyak</button>
					</div>
				</div>
				<div class="card-body p-1">
					<div class="table-responsive">
						<table class="table table-striped table-md" id="table">
							<tr>
								<th>
									<div class="custom-control custom-checkbox">
										<input type="checkbox" class="custom-control-input" id="check-all">
										<label class="custom-control-label" for="check-all"></label>
									</div>
								</th>
								<th>Kode Lokasi</th>
								<th>Kode Barang</th>
								<th>Nama Barang</th>
								<th>Bidang</th>
								<th>Asal Usul</th>
								<th>Kondisi</th>
								<th>Harga</th>
								<th>Masa Manfaat</th>
								<th>Action</th>
							</tr>
							@foreach ($items as $key => $item)
							<tr>
								<td>
									<div class="custom-control custom-checkbox">
										<input type="checkbox" class="custom-control-input" id="customCheck{{ $item->IDPemda }}" name="selected_items[]" value="{{ $item->IDPemda }}">
										<label class="custom-control-label" for="customCheck{{ $item->IDPemda }}"></label>
									</div>
								</td>
								<td>
									{{ str_pad($item->Kd_Pemilik, 2, "0", STR_PAD_LEFT) .'.'. 
									str_pad($item->Kd_Prov, 2, "0", STR_PAD_LEFT) .'.'. 
									str_pad($item->Kd_Kab_Kota, 2, "0", STR_PAD_LEFT) .'.'. 
									str_pad($item->Kd_Bidang, 2, "0", STR_PAD_LEFT) .'.'. 
									str_pad($item->Kd_Unit, 2, "0", STR_PAD_LEFT) .'.'. 
									date('y', strtotime($item->Tgl_Pembukuan)) .'.'. 
									str_pad($item->Kd_Sub, 2, "0", STR_PAD_LEFT) .'.'.
									$item->Kd_UPB }}
								</td>
								<td>
									{{ str_pad($item->Kd_Aset1, 2, "0", STR_PAD_LEFT) .'.'.
									str_pad($item->Kd_Aset2, 2, "0", STR_PAD_LEFT) .'.'.
									str_pad($item->Kd_Aset3, 2, "0", STR_PAD_LEFT) .'.'.
									str_pad($item->Kd_Aset4, 2, "0", STR_PAD_LEFT) .'.'.
									str_pad($item->Kd_Aset5, 2, "0", STR_PAD_LEFT)  .'.'.
									str_pad($item->No_Register, 4, "0", STR_PAD_LEFT) }}
								</td>
								<td>{{ $item->Nm_Aset5 }}</td>
								<td>{{ $item->field->Nm_Bidang }}</td>
								<td>{{ $item->Asal_usul }}</td>
								<td>
									@if($item->Kondisi == 1)
										<span class="badge badge-success">Baik</span>
									@elseif($item->Kondisi == 2)
										<span class="badge badge-warning">Kurang Baik</span>
									@elseif($item->Kondisi == 3)
										<span class="badge badge-danger">Rusak Berat</span>
									@elseif($item->Kondisi == 4)
										<span class="badge badge-info">Hilang</span>
									@elseif($item->Kondisi == 5)
										<span class="badge badge-light">Tidak Diketemukan</span>
									@elseif($item->Kondisi == 3)
										<span class="badge badge-dark">Lainnya</span>
									@endif
								</td>
								<td>{{ $item->Harga }}</td>
								<td>{{ $item->Masa_Manfaat }}</td>
								<td>
									<a target="_blank" href="{{ route('print-qrcode', $item->IDPemda) }}" class="btn btn-warning btn-icon icon-left"><i class="fas fa-print"></i> Cetak</a>
								</td>
							</tr>
							@endforeach
						</table>
					</div>
				</div>
			</form>

			@if($_SERVER['REQUEST_METHOD'] === 'GET')
				<div class="card-footer bg-whitesmoke">
					<nav class="d-inline-block">
						{{ $items->appends(request()->query())->links() }}
					</nav>
				</div>
			@endif
		</div>
	</div>
  </section>
@endsection

@push('styles')
<link rel="stylesheet" href="{{ asset('stisla/node_modules/bootstrap-daterangepicker/daterangepicker.css') }}">
@endpush

@push('scripts')
<script src="{{ asset('stisla/node_modules/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
<script type="text/javascript">
	$(document).ready(function(){
		$("#check-all").click(function(){
			$('#table').find('input[type=checkbox]').prop('checked', this.checked);
		});     	
	});
</script>
@endpush