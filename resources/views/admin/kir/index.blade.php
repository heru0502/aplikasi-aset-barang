@extends('templates.default')

@section('content')
<section class="section">
    <div class="section-header">
		<h1>Kartu Inventaris Ruang</h1>
	</div>
	
	<div class="section-body">
		<h2 class="section-title">Filter Aset</h2>
		<p class="section-lead">Isi parameter dibawah sesuai dengan aset barang apa yang ingin anda cari.</p>
		<div class="card">
			<div class="card-header">
				<h4>Filter</h4>
			</div>

			<div class="card-body p-3">
				<filter-component></filter-component>
			</div>
		</div>
	</div>

    <div class="section-body">
		<h2 class="section-title">Daftar Aset</h2>
		<p class="section-lead">Semua dafar aset maupun berdasarkan filter tertentu.</p>
		<div class="card">

			<form action="{{ route('print-qrcode-batch') }}" method="post" target="_blank">
				@csrf

				<div class="card-header">
					<div class="row">
						<button class="btn btn-warning" type="submit"><i class="fas fa-print"></i> Cetak Banyak</button>
					</div>
				</div>
				<div class="card-body p-1">
					<div class="table-responsive">
						<table class="table table-striped table-md">
							<tr>
								<th>#</th>
								<th>Kode Lokasi</th>
								<th>Kode Barang</th>
								<th>Nama Barang</th>
								<th>Bidang</th>
								<th>Asal Usul</th>
								<th>Kondisi</th>
								<th>Harga</th>
								<th>Masa Manfaat</th>
								<th>Action</th>
							</tr>
							@foreach ($items as $key => $item)
							<tr>
								<td>
									<div class="custom-control custom-checkbox">
										<input type="checkbox" class="custom-control-input" id="customCheck{{ $item->IDPemda }}" name="selected_items[]" value="{{ $item->IDPemda }}">
										<label class="custom-control-label" for="customCheck{{ $item->IDPemda }}"></label>
									</div>
								</td>
								<td>
									{{ str_pad($item->Kd_Pemilik, 2, "0", STR_PAD_LEFT) .'.'. 
									str_pad($item->Kd_Prov, 2, "0", STR_PAD_LEFT) .'.'. 
									str_pad($item->Kd_Kab_Kota, 2, "0", STR_PAD_LEFT) .'.'. 
									str_pad($item->Kd_Bidang, 2, "0", STR_PAD_LEFT) .'.'. 
									str_pad($item->Kd_Unit, 2, "0", STR_PAD_LEFT) .'.'. 
									date('y', strtotime($item->Tgl_Pembukuan)) .'.'. 
									str_pad($item->Kd_Sub, 2, "0", STR_PAD_LEFT) .'.'.
									$item->Kd_UPB }}
								</td>
								<td>
									{{ str_pad($item->Kd_Aset1, 2, "0", STR_PAD_LEFT) .'.'.
									str_pad($item->Kd_Aset2, 2, "0", STR_PAD_LEFT) .'.'.
									str_pad($item->Kd_Aset3, 2, "0", STR_PAD_LEFT) .'.'.
									str_pad($item->Kd_Aset4, 2, "0", STR_PAD_LEFT) .'.'.
									str_pad($item->Kd_Aset5, 2, "0", STR_PAD_LEFT)  .'.'.
									str_pad($item->No_Register, 4, "0", STR_PAD_LEFT) }}
								</td>
								<td>{{ $item->Nm_Aset5 }}</td>
								<td>{{ $item->field->Nm_Bidang }}</td>
								<td>{{ $item->Asal_usul }}</td>
								<td>{{ $item->Kondisi }}</td>
								<td>{{ $item->Harga }}</td>
								<td>{{ $item->Masa_Manfaat }}</td>
								<td>
									<a target="_blank" href="{{ route('print-qrcode', $item->IDPemda) }}" class="btn btn-warning btn-icon icon-left"><i class="fas fa-print"></i> Cetak</a>
								</td>
							</tr>
							@endforeach
						</table>
					</div>
				</div>
			</form>

		</div>
	</div>
  </section>
@endsection

@push('styles')
<link rel="stylesheet" href="{{ asset('stisla/node_modules/bootstrap-daterangepicker/daterangepicker.css') }}">
@endpush

@push('scripts')
<script src="{{ asset('stisla/node_modules/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
@endpush