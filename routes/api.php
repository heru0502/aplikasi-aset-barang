<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('filter', 'Api\FilterController@index');
Route::get('filter/get-sectors', 'Api\FilterController@getSectors');
Route::get('filter/get-units/{Kd_Bidang}', 'Api\FilterController@getUnits');
Route::get('filter/get-sub-units/{Kd_Bidang}/{Kd_Unit}', 'Api\FilterController@getSubUnits');
Route::get('filter/get-upb/{Kd_Bidang}/{Kd_Unit}/{Kd_Sub}', 'Api\FilterController@getUPB');
Route::get('filter/get-rooms/{Kd_Bidang}/{Kd_Unit}/{Kd_Sub}/{Kd_UPB}', 'Api\FilterController@getRooms');
