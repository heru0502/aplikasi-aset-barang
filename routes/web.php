<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::get('/', function() {
    return redirect()->route('kib-b');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
// Route::get('/item', 'ItemController@index')->name('item');
// Route::post('/item/search', 'ItemController@search')->name('item.search');
// Route::get('/item/print-qrcode/{id}', 'ItemController@printQrcode')->name('print-qrcode');
// Route::post('/item/print-qrcode-batch', 'ItemController@printQrcodeBatch')->name('print-qrcode-batch');

Route::prefix('admin')->namespace('Admin')->group(function() {
    Route::get('/kib-b', 'KibBController@index')->name('kib-b');
    Route::post('/kib-b/search', 'KibBController@search')->name('kib-b.search');
    Route::get('/kib-b/print-qrcode/{id}', 'KibBController@printQrcode')->name('print-qrcode');
    Route::post('/kib-b/print-qrcode-batch', 'KibBController@printQrcodeBatch')->name('print-qrcode-batch');

    // Route::get('/kir', 'KirController@index')->name('kir');
    Route::get('/kir', 'KibBController@index')->name('kir');
    Route::post('/kir/search', 'KibBController@search')->name('kir.search');
});

// Route::get('/item/convert-date', 'ItemController@convert_date');

Route::get('/logout', '\App\Http\Controllers\Auth\LoginController@logout')->name('logout');
